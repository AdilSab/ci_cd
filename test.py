import unittest

import project


class TestProject(unittest.TestCase):
    def test_is_prime(self):
        self.assertFalse(project.is_prime(5))
        self.assertTrue(project.is_prime(2))
        self.assertTrue(project.is_prime(3))
        self.assertFalse(project.is_prime(8))
        self.assertFalse(project.is_prime(10))
        self.assertTrue(project.is_prime(7))
        self.assertEqual(project.is_prime(-3),"Negative numbers are not allowed")


if __name__ == '__main__':
    unittest.main()
